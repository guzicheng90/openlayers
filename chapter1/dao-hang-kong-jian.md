# 导航滚动条控件   
## 说明：   
用Openlayers自带zoomslider控件实现，ol.map在加载时会默认加载三个常用的控件（ol.control.defaults），分别是：
* 缩放控件（ol.control.Zoom）
* 旋转控件（ol.control.Roate）
* 图层数据源属性控件（ol.control.Attribution）   
首先，加载导航滚动条控件前，先把默认控件清除，再添加。

## 调用方式：   
样式：   
```
/* 缩放滑块控件ZoomSlider的样式，放置到缩放按钮之间实现导航条功能*/
#mapCon .ol-zoom .ol-zoom-out {
margin-top: 204px;
}

#mapCon .ol-zoomslider {
background-color: #87CEFA;
top: 2.3em;
}
```   

## 核心源码：   
清除所有的控件，主要是默认控件     

```
//清除所有控件，按需加载
var ctls = map.getControls();
let ctlsLength = ctls.getArray().length;
for (let i = 0; i < ctlsLength; i++) {
map.removeControl(ctls.getArray()[0]);
}

```   

加载导航滚动条控件   
```
//zoomslider
var zoomSliderControl = new ol.control.ZoomSlider({});
map.addControl(zoomSliderControl);
```

## 效果图：    
![导航滚动条控件-效果图](/assets/导航滚动条控件-效果图.png)   
 
## 全部功能参考：    

```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>导航滚动条控件</title>
    <!--页面通用样式-->
    <style type="text/css">
        html {
            height: 100%
        }

        body {
            height: 100%;
            margin: 0px;
            padding: 0px
        }

        #container {
            height: 100%
        }

        #map {
            width: 100%;
            height: 100%;
            position: absolute;
        }
    </style>
    <style>
        /* 缩放滑块控件ZoomSlider的样式，放置到缩放按钮之间实现导航条功能*/
        #mapCon .ol-zoom .ol-zoom-out {
            margin-top: 204px;
        }

        #mapCon .ol-zoomslider {
            background-color: #87CEFA;
            top: 2.3em;
        }
    </style>
    <link href="https://cdn.bootcss.com/openlayers/4.6.5/ol.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://cdn.bootcss.com/openlayers/4.6.5/ol.js"></script>
</head>

<body>
    <div id="menu">
    </div>
    <div id="mapCon" style="width: 100%; height: 100%; position: absolute;">
    </div>
    <script type="text/javascript">

        //初始化
        init();

        /**
          *加载底图
          **/
        function init() {
            // 创建地图
            map = new ol.Map({
                // 设置地图图层
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.TileWMS({
                            crossOrigin: 'anonymous', //跨域
                            url: "http://10.19.151.238:8080/geoserver/gas/ows",
                            wrapX: false,  //是否可以重复平铺
                            params: {
                                FORMAT: 'image/png',
                                VERSION: "1.1.0", //WMS版本号
                                tiled: true, //是否是切片
                                STYLES: '',
                                LAYERS: "gas:t_area"//服务图层名
                            },
                        }),
                    })
                ],
                // 设置显示地图的视图
                view: new ol.View({
                    center: [12976694.29785, 4743563.56400],    // 定义地图显示中心于经度0度，纬度0度处
                    zoom: 9,
                    minZoom: 0,
                    maxZoom: 19
                }),
                // 让id为map的div作为地图的容器
                target: 'mapCon'
            });

            //清除所有控件，按需加载
            var ctls = map.getControls();
            let ctlsLength = ctls.getArray().length;
            for (let i = 0; i < ctlsLength; i++) {
                map.removeControl(ctls.getArray()[0]);
            }

            //zoomslider
            var zoomSliderControl = new ol.control.ZoomSlider({});
            map.addControl(zoomSliderControl);
        }
    </script>
</body>

</html>
```