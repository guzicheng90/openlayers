# 图层选择控件   
## 调用方式：   
HTML结构：   

```
<div id="mapCon" style="width: 100%; height: 100%; position: absolute;">
  <div id="layerControl" class="layerControl">
    <div class="title"><label>图层列表</label></div>
    <ul id="layerTree" class="layerTree"></ul>
  </div>
</div>
```   

样式：   
```
    <style>
        /* 图层控件层样式设置 */
        .layerControl {
            position: absolute;
            bottom: 5px;
            min-width: 200px;
            max-height: 200px;
            right: 0px;
            top: 5px;
            z-index: 2001;
            /*在地图容器中的层，要设置z-index的值让其显示在地图上层*/
            color: #ffffff;
            background-color: #4c4e5a;
            border-width: 10px;
            /*边缘的宽度*/
            border-radius: 10px;
            /*圆角的大小 */
            border-color: #000 #000 #000 #000;
            /*边框颜色*/
        }

        .layerControl .title {
            font-weight: bold;
            font-size: 15px;
            margin: 10px;
        }

        .layerTree li {
            list-style: none;
            margin: 5px 10px;
        }

        /* 鼠标位置控件层样式设置 */
        #mouse-position {
            float: left;
            position: absolute;
            bottom: 5px;
            width: 200px;
            height: 20px;
            z-index: 2000;
            /*在地图容器中的层，要设置z-index的值让其显示在地图上层*/
        }
    </style>
```   

Javascript调用：     
     
```
//实例化
var layerControl = new LayerControl();
//加载图层列表数据
layerControl.loadLayersControl(map, "layerTree", "layerName");
```   

## 接口源码：   

```
/**
 * @fileOverview 图层控制类
 */
LayerControl = function () {
    /**
     * 加载图层列表数据
     * @param {ol.Map} map 地图对象
     * @param {string} id 图层列表容器ID
     * @param {string} layerName 图层名称，用于显示在窗口
     */
    this.loadLayersControl = function (map, id, _layerName) {        
        var layer = new Array(); //map中的图层数组
        var layerName = new Array(); //图层名称数组
        var layerVisibility = new Array(); //图层可见属性数组

        var treeContent = document.getElementById(id); //图层目录容器

        var layers = map.getLayers(); //获取地图中所有图层
        for (var i = 0; i < layers.getLength(); i++) {
            //获取每个图层的名称、是否可见属性
            layer[i] = layers.item(i);
            //排除临时图层。如果图层没有layerId则不加
            if (!layer[i].get('layerId')) {
                return;
            }
            layerName[i] = layer[i].get(_layerName) ? layer[i].get(_layerName) : layer[i].get('layerName');
            layerVisibility[i] = layer[i].getVisible();

            //新增li元素，用来承载图层项
            var elementLi = document.createElement('li');
            treeContent.appendChild(elementLi); // 添加子节点
            //创建复选框元素
            var elementInput = document.createElement('input');
            elementInput.type = "checkbox";
            elementInput.name = "layers";
            elementLi.appendChild(elementInput);
            //创建label元素
            var elementLable = document.createElement('label');
            elementLable.className = "layer";
            //设置图层名称
            setInnerText(elementLable, layerName[i]);
            elementLi.appendChild(elementLable);

            //设置图层默认显示状态
            if (layerVisibility[i]) {
                elementInput.checked = true;
            }
            addChangeEvent(elementInput, layer[i]); //为checkbox添加变更事件                                         
        }
    }
    /**
     * 为checkbox元素绑定变更事件
     * @param {input} element checkbox元素
     * @param {ol.layer.Layer} layer 图层对象
     */
    function addChangeEvent(element, layer) {
        element.onclick = function () {
            if (element.checked) {
                layer.setVisible(true); //显示图层
            } else {
                layer.setVisible(false); //不显示图层
            }
        };
    }
    /**
     * 动态设置元素文本内容（兼容）
     */
    function setInnerText(element, text) {
        if (typeof element.textContent == "string") {
            element.textContent = text;
        } else {
            element.innerText = text;
        }
    }
}
```   

## 全部功能参考：    

```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>图层控制</title>
    <style type="text/css">
        html {
            height: 100%
        }

        body {
            height: 100%;
            margin: 0px;
            padding: 0px
        }

        #container {
            height: 100%
        }

        #map {
            width: 100%;
            height: 100%;
            position: absolute;
        }

        /* 图层控件层样式设置 */

        .layerControl {
            position: absolute;
            bottom: 5px;
            min-width: 200px;
            max-height: 200px;
            right: 0px;
            top: 5px;
            z-index: 2001;
            /*在地图容器中的层，要设置z-index的值让其显示在地图上层*/
            color: #ffffff;
            background-color: #4c4e5a;
            border-width: 10px;
            /*边缘的宽度*/
            border-radius: 10px;
            /*圆角的大小 */
            border-color: #000 #000 #000 #000;
            /*边框颜色*/
        }

        .layerControl .title {
            font-weight: bold;
            font-size: 15px;
            margin: 10px;
        }

        .layerTree li {
            list-style: none;
            margin: 5px 10px;
        }

        /* 鼠标位置控件层样式设置 */

        #mouse-position {
            float: left;
            position: absolute;
            bottom: 5px;
            width: 200px;
            height: 20px;
            z-index: 2000;
            /*在地图容器中的层，要设置z-index的值让其显示在地图上层*/
        }
    </style>
    <link href="../lib/css/ol.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../lib/ol/ol-debug.js"></script>
</head>

<body>
    <div id="menu">
    </div>
    <div id="mapCon" style="width: 100%; height: 100%; position: absolute;">
        <div id="layerControl" class="layerControl">
            <div class="title"><label>图层列表</label></div>
            <ul id="layerTree" class="layerTree"></ul>
        </div>
    </div>
    <script type="text/javascript">

        var layer = new Array(); //map中的图层数组
        var layerName = new Array(); //图层名称数组
        var layerVisibility = new Array(); //图层可见属性数组

        //初始化
        init();

        /**
          *加载3857坐标系底图
          **/
        function init() {
            /*Geoserver服务*/
            var baseMapServer =
            {
                url: 'http://10.19.151.238:8080/geoserver/gas/ows',
                pipeUrl: 'http://10.19.151.238:8080/geoserver/NA/wms',
                typeName: 'gas:t_area',
                layerId: 'gas:t_area',
                layerName: '区县边界'
            };

            // 创建地图
            map = new ol.Map({
                // 设置地图图层
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.TileWMS({
                            crossOrigin: 'anonymous', //跨域
                            url: "http://10.19.151.238:8080/geoserver/NA/wms",
                            wrapX: false,  //是否可以重复平铺
                            params: {
                                FORMAT: 'image/png',
                                VERSION: "1.1.0", //WMS版本号
                                tiled: true, //是否是切片
                                STYLES: '',
                                LAYERS: "NA:zy"//服务图层名
                            },
                        }),
                        //图层名称，用于图层控制面板显示
                        layerName: "管网"
                    }),
                    new ol.layer.Tile({
                        source: new ol.source.TileWMS({
                            crossOrigin: 'anonymous', //跨域
                            url: "http://10.19.151.238:8080/geoserver/NA/wms",
                            wrapX: false,  //是否可以重复平铺
                            params: {
                                FORMAT: 'image/png',
                                VERSION: "1.1.0", //WMS版本号
                                tiled: true, //是否是切片
                                STYLES: '',
                                LAYERS: "NA:fm"//服务图层名
                            },
                        }),
                        //图层名称，用于图层控制面板显示
                        layerName: "阀门"
                    })
                ],
                // 设置显示地图的视图
                view: new ol.View({
                    center: [12976694.29785, 4743563.56400],    // 定义地图显示中心于经度0度，纬度0度处
                    zoom: 9,
                    minZoom: 0,
                    maxZoom: 19
                }),
                // 让id为map的div作为地图的容器
                target: 'mapCon'
            });

            //加载图层列表数据
            loadLayersControl(map, "layerTree");
        }

        /**
         * 加载图层列表数据
         * @param {ol.Map} map 地图对象
         * @param {string} id 图层列表容器ID
         */
        function loadLayersControl(map, id) {
            var treeContent = document.getElementById(id); //图层目录容器

            var layers = map.getLayers(); //获取地图中所有图层
            for (var i = 0; i < layers.getLength(); i++) {
                //获取每个图层的名称、是否可见属性
                layer[i] = layers.item(i);
                layerName[i] = layer[i].get('layerName');
                layerVisibility[i] = layer[i].getVisible();

                //新增li元素，用来承载图层项
                var elementLi = document.createElement('li');
                treeContent.appendChild(elementLi); // 添加子节点
                //创建复选框元素
                var elementInput = document.createElement('input');
                elementInput.type = "checkbox";
                elementInput.name = "layers";
                elementLi.appendChild(elementInput);
                //创建label元素
                var elementLable = document.createElement('label');
                elementLable.className = "layer";
                //设置图层名称
                setInnerText(elementLable, layerName[i]);
                elementLi.appendChild(elementLable);

                //设置图层默认显示状态
                if (layerVisibility[i]) {
                    elementInput.checked = true;
                }
                addChangeEvent(elementInput, layer[i]); //为checkbox添加变更事件                                         
            }
        }
        /**
         * 为checkbox元素绑定变更事件
         * @param {input} element checkbox元素
         * @param {ol.layer.Layer} layer 图层对象
         */
        function addChangeEvent(element, layer) {
            element.onclick = function () {
                if (element.checked) {
                    layer.setVisible(true); //显示图层
                } else {
                    layer.setVisible(false); //不显示图层
                }
            };
        }
        /**
         * 动态设置元素文本内容（兼容）
         */
        function setInnerText(element, text) {
            if (typeof element.textContent == "string") {
                element.textContent = text;
            } else {
                element.innerText = text;
            }
        }
    </script>
</body>

</html>
```