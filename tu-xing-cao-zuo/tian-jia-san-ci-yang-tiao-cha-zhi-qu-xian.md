# 添加三次样条插值曲线      
## 说明：   
项目中需要一个需求，需要做如下显示效果，用于展现关键人员的行动路线。      
![需求效果](/assets/需求效果.png)   
看到这个需求，第一个想到的就是两点之间绘制贝塞尔曲线，我用原来做过的迁徙图尝试模拟了一下效果：   
![迁徙图存在问题](/assets/迁徙图存在问题.png)     
最后发现这种设计并不能满足需求，一方面两点间独立绘制曲线，不可避免会出现交叉；另一方面，没法控制他们的曲率，造成形成尖锐角的情况。   
这时候[三次样条插值曲线](https://baike.baidu.com/item/%E4%B8%89%E6%AC%A1%E6%A0%B7%E6%9D%A1%E6%8F%92%E5%80%BC/3476729?fr=aladdin)进入了我的视野，它可以基于所有的点数据，进行曲率运算，避免了上面提到的交叉和曲率的问题。而且意外的是，openlayers的扩展包ol-ext.js就有这个功能的实现方式，名字叫[Cardinal spline](http://viglino.github.io/ol-ext/examples/geom/map.geom.cspline.html)（不用自己写算法真是好）。   

## 调用方式：   

Javascript调用：     
     
```
//实例化
var addShapeTool = new AddShapeTool();
var arrlines = [
   [12976976.994901, 4794518.558],
   [12976955.766187, 4794544.35],
   [12976978.776543, 4794480.985],
   [12976694.29785, 4743563.56400],
   [12956534, 4711272],
   [12951934.29785, 4719272.56400]
];
var arrowStyle = { scale: 1, color: '#00ff00', width: 1 };
var lineStyle = { color: '#0000ff', width: 2 };
addShapeTool.addCspline(map, arrlines, arrowStyle, lineStyle);
```   

## 核心源码：   
将线要素转换为样条曲线      

```
var csp = feature.getGeometry().cspline(opt);

```   

计算曲线中点，绘制箭头。可能有部分点无法构造样条曲线，则默认连接两点，取直线中点。    
```
/***** 获取中心点来标注 *****/
var startIndex = csp.getCoordinates().findIndex(function (e) {
   return JSON.stringify(e) == JSON.stringify(_start);
});
var endIndex = csp.getCoordinates().findIndex(function (e) {
   return JSON.stringify(e) == JSON.stringify(_end);
});

//如果两点间无法构建曲线，则默认连接两点
if (startIndex < 0 || endIndex < 0) {
   var lineStr = new ol.geom.LineString([_start, _end]);
   styles.push(new ol.style.Style({
      geometry: lineStr,
      stroke: new ol.style.Stroke({ color: _lineStyle.color, width: _lineStyle.width })
   }));
   start = _start;
   end = [(_start[0] + _end[0]) / 2, (_start[1] + _end[1]) / 2];;
}
else {
   midIndex = Math.ceil((endIndex + startIndex - 1) / 2);
   start = csp.getCoordinates()[midIndex];
   end = csp.getCoordinates()[midIndex + 1];
}
```

## 效果图：    
![效果图](/assets/效果图.png)   
 
## 全部功能参考：    

```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>三次样条插值曲线</title>
    <!--页面通用样式-->
    <style type="text/css">
        html {
            height: 100%
        }

        body {
            height: 100%;
            margin: 0px;
            padding: 0px
        }

        #container {
            height: 100%
        }

        #map {
            width: 100%;
            height: 100%;
            position: absolute;
        }
    </style>
    <link href="https://cdn.bootcss.com/openlayers/4.6.5/ol.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://cdn.bootcss.com/openlayers/4.6.5/ol.js"></script>
    <link href="../lib/olext/ol-ext.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../lib/olext/ol-ext.js"></script>
</head>

<body>
    <div id="menu">
    </div>
    <div id="mapCon" style="width: 100%; height: 100%; position: absolute;">
    </div>
    <script type="text/javascript">

        //初始化
        init();

        /**
        *加载底图
        **/
        function init() {
            // 创建地图
            map = new ol.Map({
                // 设置地图图层
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.TileWMS({
                            crossOrigin: 'anonymous', //跨域
                            url: "http://10.19.151.238:8080/geoserver/gas/ows",
                            wrapX: false, //是否可以重复平铺
                            params: {
                                FORMAT: 'image/png',
                                VERSION: "1.1.0", //WMS版本号
                                tiled: true, //是否是切片
                                STYLES: '',
                                LAYERS: "gas:t_area"//服务图层名
                            },
                        }),
                    })
                ],
                // 设置显示地图的视图
                view: new ol.View({
                    center: [12976694.29785, 4743563.56400], // 定义地图显示中心于经度0度，纬度0度处
                    zoom: 9,
                    minZoom: 0,
                    maxZoom: 19
                }),
                // 让id为map的div作为地图的容器
                target: 'mapCon'
            });

            var lstring = [
                [12976976.994901, 4794518.558],
                [12976955.766187, 4794544.35],
                [12976978.776543, 4794480.985],
                [12976694.29785, 4743563.56400],
                [12956534, 4711272],
                [12951934.29785, 4719272.56400]
            ];
            var _arrowStyle = { scale: 0.5, color: '#00ff00', width: 2 };
            var _lineStyle = { color: '#00ff00', width: 2 };

            var features = new ol.Collection();
            features.push(new ol.Feature(new ol.geom.LineString(lstring)));

            var vector = new ol.layer.Vector({
                name: 'Vecteur',
                source: new ol.source.Vector({ features: features }),
                style: function (feature) {
                    var geometry = feature.getGeometry();

                    var opt = {
                        tension: 0.5,
                        pointsPerSeg: 10,
                        normalize: false
                    };
                    var csp = feature.getGeometry().cspline(opt);

                    var styles = [
                        //曲线样式
                        new ol.style.Style({
                            stroke: new ol.style.Stroke({ color: _lineStyle.color, width: _lineStyle.width }),
                            geometry: true ? csp : null
                        }),
                        //是否显示插值点
                        new ol.style.Style({
                            image: new ol.style.Circle({ stroke: new ol.style.Stroke({ color: "blue", width: 1 }), radius: 1 }),
                            geometry: false ? new ol.geom.MultiPoint(csp.getCoordinates()) : null
                        }),
                        //是否显示坐标拐点
                        new ol.style.Style({
                            image: new ol.style.Circle({ stroke: new ol.style.Stroke({ color: "red", width: 4 }), radius: 2 }),
                            geometry: false ? new ol.geom.MultiPoint(feature.getGeometry().getCoordinates()) : null
                        })
                    ];

                    geometry.forEachSegment(function (_start, _end) {
                        var midIndex;//曲线中点
                        var start;//曲线头
                        var end;//曲线尾

                        /***** 获取中心点来标注 *****/
                        var startIndex = csp.getCoordinates().findIndex(function (e) {
                            return JSON.stringify(e) == JSON.stringify(_start);
                        });
                        var endIndex = csp.getCoordinates().findIndex(function (e) {
                            return JSON.stringify(e) == JSON.stringify(_end);
                        });
                        //如果两点间无法构建曲线，则默认连接两点
                        if (startIndex < 0 || endIndex < 0) {
                            var lineStr = new ol.geom.LineString([_start, _end]);
                            styles.push(new ol.style.Style({
                                geometry: lineStr,
                                stroke: new ol.style.Stroke({ color: _lineStyle.color, width: _lineStyle.width })
                            }));
                            start = _start;
                            end = [(_start[0] + _end[0]) / 2, (_start[1] + _end[1]) / 2];;
                        }
                        else {
                            midIndex = Math.ceil((endIndex + startIndex - 1) / 2);
                            start = csp.getCoordinates()[midIndex];
                            end = csp.getCoordinates()[midIndex + 1];
                        }

                        var dx = end[0] - start[0];
                        var dy = end[1] - start[1];
                        var rotation = Math.atan2(dy, dx);

                        //箭头长度，根据线长自动计算
                        var dis = Math.sqrt(Math.pow(Math.abs(dx), 2) + Math.pow(Math.abs(dy), 2)) * _arrowStyle.scale;

                        var lineStr1 = new ol.geom.LineString([end, [end[0] - dis, end[1] + dis]]);
                        lineStr1.rotate(rotation, end);
                        var lineStr2 = new ol.geom.LineString([end, [end[0] - dis, end[1] - dis]]);
                        lineStr2.rotate(rotation, end);

                        var stroke = new ol.style.Stroke({
                            color: _arrowStyle.color,
                            width: _arrowStyle.width
                        });
                        styles.push(new ol.style.Style({
                            geometry: lineStr1,
                            stroke: stroke
                        }));
                        styles.push(new ol.style.Style({
                            geometry: lineStr2,
                            stroke: stroke
                        }));
                    });

                    return styles;
                }
            })
            map.addLayer(vector);
        }
    </script>
</body>

</html>
```