# 这里记录Gitbook使用过程中的问题
1. book.json：Gitbook的配置文件，我发现我的版本安装后并没有自动生成，所以在**SUMMARY.MD**同级目录下自己建了一个。
2. 目录折叠：用**expandable-chapters-small**插件实现，在**book.json**中添加如下代码后，执行`gitbook install`
```
{
    "plugins": ["expandable-chapters-small"],
    "pluginsConfig": {
        "expandable-chapters-small":{}
    }
}
```
3. gitbook命令：   
 * 安装gitbook：`npm install gitbook-cli -g`      
 * 查看gitbook版本：`gitbook -V`     
 * 移动到位置：`gitbook init`   
 * 初始化并新建：`gitbook init ./99gitbook`     
 * 进入新建文件夹位置：`cd 99gitbook`     
 * 新建框架：`gitbook build`     
 * 创建书并预览：`gitbook serve`       
 * 更改端口：`gitbook serve --port 8899`     

