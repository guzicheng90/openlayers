# Openlayers示例

记录Openlayers做过的有趣功能，从基础到高级。这里用的版本是openlayers4.6.5
*本文默认本地路径：C:\Users\guzicheng\GitBook\Library\Import*

##用到的工具清单：   
记录一下用到的工具，哪怕用到的再小也记录一下，也算是备忘。
- 1.Geoserver：地图引擎，用来发布服务
- 2 VSCode：IDE开发工具
- 3.ArcMap：此处主要用来处理数据（因为我对ArcGIS系比较熟悉，其实可以用开源uDig）
- 4.PostgreSql：数据库，其中用到一些插件，也列在下方。
   - PostGIS：空间扩展，只有pg里打了这个插件，才能成为空间数据库，否则就是一个关系型数据库。
   - pgRouting：路由功能扩展，做网络分析效果很好，能实现Dijkstra算法、建立拓扑、最优路径等。
   - mysql_fdw：链接mysql数据库扩展，实现mysql与pg（foreignTable，外表）的互联。

##用到的组件清单：
虽然在各个文章中也会提及，但是在此先列个清单。
- 1.ol.js：openlayers3核心库   
 - cdn：https://cdn.bootcss.com/openlayers/4.6.5/ol.js   
- 2.ol.css：ol.js配套样式库
 - cdn：https://cdn.bootcss.com/openlayers/4.6.5/ol.css   
- 3.ol-ext.js：openlayers动画库，有很多基于openlayers的动画
- 4.ol-ext.css：ol-ext.js配套样式库
- 5.echarts.js：百度echarts库，可以结合openlayers，把样式做到地图上
 - cdn：https://cdn.jsdelivr.net/npm/echarts/dist/echarts.js
- 6.ol3Echarts.js：openlayers3与百度echarts结合的核心库
 - cdn：https://cdn.jsdelivr.net/npm/ol3-echarts/dist/ol3Echarts.js
