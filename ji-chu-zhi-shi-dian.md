# Openlayers基础知识点

### 参考网站   
[Openlayers官网教程](https://openlayers.org/en/latest/doc/tutorials/)   
[Openlayers官方API](https://openlayers.org/en/latest/apidoc/)   
[Openlayers官方示例](https://openlayers.org/en/latest/examples/)   

### 一、Openlayers介绍   
#### 什么是openlayers   
开篇不落俗套，介绍一下Openlayers。   
他是一个模块化，高性能，功能丰富的库，用于显示地图和地理空间数据并与之交互。该库具有对各种商业和免费图像和矢量图块源的内置支持，以及最流行的开放式和专有矢量数据格式。借助OpenLayers的地图投影支持，数据可以在任何投影中。   

#### 渲染器和浏览器支持   
默认情况下，OpenLayers使用性能经过优化的Canvas渲染器。   
OpenLayers在支持HTML5和ECMAScript 5的所有现代浏览器上运行。其中包括Chrome，Firefox，Safari和Edge。对于较旧的浏览器和平台，例如Internet Explorer（版本9之前的版本）和Android 4.x，polyfill，需要打包（例如，使用Babel）编译应用程序包requestAnimationFrame，Element.prototype.classList并与和的polyfill 捆绑在一起URL。   
该库适用于台式机/笔记本电脑和移动设备，并支持指针和触摸交互。   
 
### 二、Openlayers基本概念   

Openlayers的主要组成包含下面几块（官网只列了前面4种，其实我更认同网上普遍列出的6种的说法）     
    
#### 地图（Map）   
OpenLayers的核心组件是map（ol/Map），它呈现给target容器（例如div，包含地图的网页上的元素）。  
 
#### 视图（View）   
控制地图的显示，如层级、范围、中心等。 
  
#### 数据源（Source）     
数据源是指图层的源数据，每个图层绑定一个数据源。现Openlayers支持的数据源越来越多：免费和商业的地图图块服务（如OpenStreetMap或Bing），OGC源（如WMS或WMTS）以及矢量数据（如GeoJSON或KML）。   

#### 图层（Layer）   
图层是数据可视的一种表现形式，使数据源source能图形化显示、管理，并支持多个图层叠加。    
Openlayers官方解释有四种基本类型的图层：
* ol/layer/Tile -渲染源，这些源在按缩放级别针对特定分辨率组织的网格中提供平铺图像。   
* ol/layer/Image -渲染源，以任意范围和分辨率提供地图图像。    
* ol/layer/Vector -在客户端渲染矢量数据。      
* ol/layer/VectorTile -渲染作为矢量切片提供的数据。 
      
#### 控件（Control）   
控件是可见的窗口小部件，在屏幕上的固定位置具有DOM元素。通常放在地图容器中，不需要二次开发，直接可以与地图进行交互。Openlayers封装了很多与地图交互的控件。    
* module:ol/control/Attribution -地图属性控件   
* module:ol/control/FullScreen -全屏控件   
* module:ol/control/MousePosition -鼠标位置控件   
* module:ol/control/OverviewMap -鹰眼控件
* module:ol/control/Rotate -地图旋转控件
* module:ol/control/ScaleLine -比例尺控件
* module:ol/control/ZoomSlider -缩放滚动条控件
* module:ol/control/ZoomToExtent -缩放到范围控件
* module:ol/control/Zoom -放大缩小控件    
  
地图加载时，会默认加载3个控件   
![地图默认控件](/assets/地图默认控件.png)

#### 交互（Interaction）    
与地图的交互事件，他看不到，却控制着与地图相关的操作。如：放大、缩小、漫游、点击等。Openlayers官方提供了交互有：   
* module:ol/interaction/DoubleClickZoom -双击缩放事件
* module:ol/interaction/DragAndDrop -拖放事件   
* module:ol/interaction/KeyboardPan -键盘方式平移事件
* module:ol/interaction/KeyboardZoom -键盘方式缩放事件
* module:ol/interaction/MouseWheelZoom -鼠标滚轮缩放事件
* module:ol/interaction/Pointer -基类，鼠标事件
* module:ol/interaction/Select -选择要素事件    
     
地图加载时，会默认加载9个交互事件   
![地图默认交互事件](/assets/地图默认交互事件.png)

  

